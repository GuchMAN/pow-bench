package benches;

import org.springframework.stereotype.Component;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

@Component
public class SimpleForkJoinPoolStatic implements PowCalculator {
    public static ForkJoinPool pool;

    @Override
    public int[] calc(int[] array, int pow) {
        Executor executor = new Executor(array, pow);
        return executor.execute();
    }

    public class Executor {
        private int[] array;
        private int[] result;
        private int pow;

        public Executor(int[] array, int pow ) {
            this.array = array;
            this.pow = pow;
        }

        public int[] execute() {
            result = new int[array.length];
            PowCalculatorRunnable executor = new PowCalculatorRunnable(0, array.length);
            pool.invoke(executor);
            return result;
        }

        public class PowCalculatorRunnable extends RecursiveAction {

            private static final int THRESHOLD = 10_000;
            final int from;
            final int length;

            public PowCalculatorRunnable(int from, int length) {
                this.from = from;
                this.length = length;
            }

            @Override
            protected void compute() {
                if (length <= THRESHOLD) {
                    for (int i = from; i < from + length; ++i) {
                        result[i] = (int) Math.pow(array[i], pow);
                    }
                    return;
                }

                int half = this.length / 2;
                RecursiveAction taskLeft = new PowCalculatorRunnable(from, half);
                RecursiveAction taskRight = new PowCalculatorRunnable(from + half, half + this.length % 2);

                invokeAll(taskLeft, taskRight);
            }
        }
    }
}

