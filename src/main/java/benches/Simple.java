package benches;

import org.springframework.stereotype.Component;

@Component
public class Simple implements PowCalculator {
    public int[] calc(int[] array, int pow) {
        int[] result = new int[array.length];
        for (int i = 0; i < array.length; ++i) {
            result[i] = (int) Math.pow(array[i], pow);
        }
        return result;
    }
}
