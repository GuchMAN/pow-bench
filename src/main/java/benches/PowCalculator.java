package benches;

public interface PowCalculator {
    int[] calc(int array[], final int pow);
}
