package benches;

import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class StreamsSimple implements PowCalculator {
    @Override
    public  int[] calc(int array[], final int pow){
        return Arrays.stream(array).map(n -> (int)Math.pow(n, pow)).toArray();
    }
}
