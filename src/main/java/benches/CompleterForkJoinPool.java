package benches;

import org.springframework.stereotype.Component;

import java.util.concurrent.CountedCompleter;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

@Component
public class CompleterForkJoinPool implements PowCalculator {

    private final int threshold;

    public CompleterForkJoinPool(int threshold) {
        this.threshold = threshold;
    }

    @Override
    public int[] calc(int[] array, int pow) {
        Executor executor = new Executor(array, pow);
        return executor.execute();
    }

    public class Executor {
        private int[] array;
        private int[] result;
        private int pow;

        public Executor(int[] array, int pow) {
            this.array = array;
            this.pow = pow;
        }

        public int[] execute() {
            result = new int[array.length];
            PowCalculatorRunnable executor = new PowCalculatorRunnable(0, array.length, null);
            ForkJoinPool pool = new ForkJoinPool();
            pool.invoke(executor);
            return result;
        }

        public class PowCalculatorRunnable extends CountedCompleter<Void> {

            final int from;
            final int length;

            public PowCalculatorRunnable(int from, int length, CountedCompleter parent) {
                super(parent);
                this.from = from;
                this.length = length;
            }


            @Override
            public void compute() {
                if (length <= threshold) {
                    for (int i = from; i < from + length; ++i) {
                        result[i] = (int) Math.pow(array[i], pow);
                    }
                    propagateCompletion();
                    return;
                }

                int half = this.length / 2;
                CountedCompleter taskLeft = new PowCalculatorRunnable(from, half, this);
                CountedCompleter taskRight = new PowCalculatorRunnable(from + half, half + this.length % 2, this);
                setPendingCount(1);
                taskLeft.fork();
                taskRight.compute();
            }
        }
    }
}

