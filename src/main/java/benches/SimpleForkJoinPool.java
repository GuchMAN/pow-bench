package benches;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

import org.springframework.stereotype.Component;

@Component
public class SimpleForkJoinPool implements PowCalculator {

    private final int threshold;

    public SimpleForkJoinPool(int threshold) {
        this.threshold = threshold;
    }

    @Override
    public int[] calc(int[] array, int pow) {
        Executor executor = new Executor(array, pow);
        return executor.execute();
    }

    public class Executor {
        private int[] array;
        private int[] result;
        private int pow;

        public Executor(int[] array, int pow) {
            this.array = array;
            this.pow = pow;
        }

        public int[] execute() {
            result = new int[array.length];
            PowCalculatorRunnable executor = new PowCalculatorRunnable(0, array.length);
            ForkJoinPool pool = new ForkJoinPool();
            pool.invoke(executor);
            return result;
        }

        public class PowCalculatorRunnable extends RecursiveAction {

            final int from;
            final int length;

            public PowCalculatorRunnable(int from, int length) {
                this.from = from;
                this.length = length;
            }

            @Override
            protected void compute() {
                if (length <= threshold) {
                    for (int i = from; i < from + length; ++i) {
                        result[i] = (int) Math.pow(array[i], pow);
                    }
                    return;
                }

                int half = this.length / 2;
                RecursiveAction taskLeft = new PowCalculatorRunnable(from, half);
                RecursiveAction taskRight = new PowCalculatorRunnable(from + half, half + this.length % 2);

                invokeAll(taskLeft, taskRight);
            }
        }
    }
}

