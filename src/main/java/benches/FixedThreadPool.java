package benches;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

public class FixedThreadPool implements PowCalculator {

    public static ExecutorService executorService ;
    private int procCoef = 1;
    public FixedThreadPool(int procCoef ) {
        this.procCoef = procCoef;

    }
    public class PowCalculatorRunnable implements Runnable {

        final int from;
        final int length;
        private final int[] array;
        private final int[] result;
        private int pow;
        public PowCalculatorRunnable(int from, int length, int[] input, int[] output, int pow) {
            this.from = from;
            this.length = length;
            this.array = input;
            this.result = output;
            this.pow = pow;
        }


        @Override
        public void run() {
            for (int i = from; i < from + length; ++i) {
                result[i] = (int) Math.pow(array[i], pow);
            }
        }
    }

    @Override
    public int[] calc(int[] input, int pow) {
        int nThreads = Runtime.getRuntime().availableProcessors() * procCoef;

        int[] output = new int[input.length];
        int step = input.length / nThreads;

        try {
            int from = 0;
            List<Future<?>> futures = new ArrayList<>();
            for (int i = 0, len = step; i < nThreads - 1; ++i, from = len, len += step) {
                Future<?> submit = executorService.submit(new PowCalculatorRunnable(from, len, input, output, pow));
                futures.add(submit);
            }
            int last = input.length % nThreads;
            new PowCalculatorRunnable(from, last, input, output, pow).run();

            for (Future future : futures) {
                future.get();
            }
            executorService.shutdown();
            executorService.awaitTermination(3, TimeUnit.MINUTES);
        } catch (InterruptedException | ExecutionException e) {
            /* do nothing */
        }
        return output;
    }
}