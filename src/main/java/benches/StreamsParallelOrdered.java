package benches;

import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class StreamsParallelOrdered implements PowCalculator{
    public int[] calc(int array[], final int pow){
        return Arrays.stream(array).parallel().map(n -> (int)Math.pow(n, pow)).toArray();
    }
}
