package benches;

import java.util.Arrays;

import org.springframework.stereotype.Component;

@Component
public class StreamsParallelUnordered implements PowCalculator {
    @Override
    public  int[] calc(int array[], final int pow){
        return Arrays.stream(array).parallel().unordered().map(n -> (int)Math.pow(n, pow)).toArray();
    }
}
