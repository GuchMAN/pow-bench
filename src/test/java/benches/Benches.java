package benches;

import org.openjdk.jmh.annotations.*;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ForkJoinPool;

@State(Scope.Benchmark)
@Fork(warmups = 1, value = 5, jvmArgs = {"-Xms1G", "-Xmx1G"})
@Measurement(iterations = 5)
@Warmup(iterations = 5)
@BenchmarkMode(Mode.Throughput)
public class Benches {
    private int[] arr;

    private static int POW = 2;
    private static ExecutorService threadPool;
    private static ForkJoinPool forkJoinPool;

    @Setup
    public void init() {
        arr = new int[9_000_000];
        int nThreads = Runtime.getRuntime().availableProcessors();
        forkJoinPool= new ForkJoinPool(nThreads);
        SimpleForkJoinPoolStatic.pool = forkJoinPool;
        threadPool = Executors.newFixedThreadPool(nThreads);

    }

    @Benchmark
    public int[] simple() {
        return new Simple().calc(arr, POW);
    }

    @Benchmark
    public int[] simpleForkJoinPoolStatic() {
        return new SimpleForkJoinPoolStatic().calc(arr, POW);
    }

    @Benchmark
    public int[] fixedThreadPool() {
        FixedThreadPool.executorService = threadPool;
        return new FixedThreadPool(1).calc(arr, POW);
    }

    @TearDown
    public void tearDown() {
        threadPool.shutdownNow();
        forkJoinPool.shutdownNow();
    }

}
/*
Benches.simple                       thrpt   25  32,883 � 1,307  ops/s
Benches.streamsSimple                thrpt   25  32,949 � 1,214  ops/s
Benches.fixedThreadPool2             thrpt   25  37,107 � 1,713  ops/s
Benches.fixedThreadPool              thrpt   25  52,236 � 2,321  ops/s
Benches.simpleForkJoinPoolStatic     thrpt   25  72,865 � 3,487  ops/s
Benches.simpleForkJoinPool_5000      thrpt   25  72,910 � 3,788  ops/s
Benches.completerForkJoinPoolStatic  thrpt   25  73,036 � 3,659  ops/s
Benches.simpleForkJoinPool_10000     thrpt   25  73,367 � 3,927  ops/s
Benches.simpleForkJoinPool_20000     thrpt   25  73,375 � 3,626  ops/s
Benches.simpleForkJoinPool_50000     thrpt   25  73,387 � 3,749  ops/s
Benches.streamParallelOrdered        thrpt   25  83,146 � 3,867  ops/s
Benches.streamParallelUnordered      thrpt   25  83,425 � 3,820  ops/s
 */