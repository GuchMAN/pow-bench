package benches;


import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;


public class CalculationTest {

    private static final int SIZE = 23000;
    private static final int POW = 2;
    protected static final int INIT_VAL = 2;

    private List<PowCalculator> calculators;

    @Before
    public void init() {
        calculators = new ArrayList<>();
        calculators.addAll(
                Arrays.asList(
                        new Simple(),
                        new StreamsParallelUnordered(),
                        new StreamsParallelOrdered(),
                        new FixedThreadPool(1),
                        new SimpleForkJoinPool(10000),
                        new SimpleForkJoinPoolStatic(),
                        new CompleterForkJoinPool(10000)
                )
        );
    }

    @Test
    public void allPowerCalculate() throws Exception {
        assertFalse(calculators.isEmpty());
        for (PowCalculator calculator : calculators) {
            try {
                test(calculator);
            } catch (AssertionError e) {
                System.out.print(calculator.getClass().getSimpleName() + " error:");
                throw e;
            }
        }

    }

    private void test(PowCalculator calculator) {
        int[] input = new int[SIZE];
        Arrays.fill(input, INIT_VAL);
        int[] result = calculator.calc(input, POW);
        assertEquals(input.length, result.length);

        for (int i = 0; i < input.length; ++i) {
            assertEquals((int) Math.pow(INIT_VAL, POW), result[i]);
        }
    }
}
